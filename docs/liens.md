# Liens utiles

**Programmation / électronique :**

* [Wokwi](https://wokwi.com/) : simulation de projet électronique en ligne, permet de tester des programmes Arduino, 
Raspberry Pico, ESP32 en ligne sans avoir besoin de matériel.
* [Thonny](https://thonny.org/) : logiciel pour programmer en MicroPython
* [Arduino](https://www.arduino.cc/) : plateforme de développement électronique
* [Raspberry Pi](https://www.raspberrypi.org/) : plateforme de développement électronique
* [ChatGPT](https://chatgpt.com/) : IA pour aider à programmer en MicroPython ou C

**Impression 3D :**

* [Thingiverse](https://www.thingiverse.com/) : projet 3D à imprimer
* [CURA](https://ultimaker.com/fr/software/ultimaker-cura) : logiciel d'impression 3D
* [ScketchUp](https://www.sketchup.com/fr/try-sketchup#for-personal) : logiciel de conception 3D en ligne (gratuit)
* [FreeCAD](https://www.freecadweb.org/) : logiciel de conception 3D (gratuit)


**Projets :**

* [Robot 3D](https://docs.sunfounder.com/projects/3in1-kit/en/latest/car_project/car_project.html) : 
projet de robot 3D à imprimer et à assembler (open-source)