# Actualités du club

## Salon Angers GeekFest - 1 et 2 avril 2023

L'association sera présente au salon Angers GeekFest les 1 et 2 avril 2023 au parc des expositions d'Angers. 
Venez rencontrer l'association et ses membres sur son stand.

Plus d'informations sur l'événement : [https://angersgeekfest.com/](https://angersgeekfest.com/)

## Coupe de France de robotique junior 2023 - 18 mars 2023

L'association sera présente aux qualifications régionales de la coupe de France de Robotique le 18 mars 2023 au parc des expositions d'Angers de 10h à 18h (entrée gratuite).

[Site officiel](https://www.coupederobotique.fr/event/coupe-de-robotique-junior-pays-de-la-loire-2023/)

## Assemblée générale 2022

Le 15 octobre 2022, l'association a organisé son assemblé générale annuelle en présence de ses membres. Ce fut l'occasion de faire le bilan de l'année 2021, d'élir le bureau et de présenter les projets à venir.

## Coupe de France de robotique junior 2022

L'association sera présente aux qualifications régionales de la coupe de France de Robotique le 9 avril 2022 au parc des expositions d'Angers de 10h à 18h (entrée gratuite).

Venez rencontrer l'association sur son stand et découvrir les projets réalisés par ses membres.

[Site officiel](https://www.coupederobotique.fr/event/coupe-de-robotique-junior-pays-de-la-loire-2022/)

## Salon Angers GeekFest 2022

Venez rencontrer l'association et ses membres lors du prochain GeekFest à Angers le 2 et 3 avril 2022. Nous présentons les projets de membres : objets connectés, robotique, photographie,...

Plus d'informations sur l'événement : [https://angersgeekfest.com/](https://angersgeekfest.com/)