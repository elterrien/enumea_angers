## Raspberry Pi 3

### Quel système d'exploitation utiliser pour le module raspberry pi ?

Après plusieurs essais d'OS différents je me suis arrêté sur l'OS Raspbian Stretch qui permet d'avoir une configuration de base déjà très complète. Je ne vais pas détailler la méthode pour installer l'OS sur une carte µSD, mais laisser quelques liens utiles pour le faire. En cas de difficulté voir sur internet l'installation d'un OS sur Raspberry pi3.

Pour télécharger Raspbian Stretch, il faut suivre le lien ci-dessous.

<https://www.raspberrypi.org/downloads/raspbian/>

Il est possible avec Windows de créer une carte µSD bootable avec l'OS Raspbian Stretch, pour cela il faut utiliser [Win32DiskImager](https://sourceforge.net/projects/win32diskimager/)  pour copier l'image disque sur la carte µSD.

### Protéger le module avec un boitier

![Boitier Raspberry Pi](../images/projet/raspberry_3/boitier_rpi_3d.png)

Un boîtier protégera des risques de court-circuit ainsi que des chocs éventuels. Il y a la solution toute faite avec des boîtiers prévus pour le module ou un boîtier sur mesure réalisé avec une imprimante 3D (celui-ci a été trouvé sur le site [Thingiverse](https://www.thingiverse.com/thing:2114385)).
