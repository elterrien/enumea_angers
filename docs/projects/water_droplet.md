# Water Droplet pour la photographie

## Version n°1 du water droplet

Le système Water Droplet permet de générer une goutte d'eau qui va tomber dans un récipient rempli de liquide. Juste après l'impact de la goutte d'eau avec le liquide il y a en premier la formation d'un cratère et ensuite la création d'une colonne d'eau. 

Le système Water Droplet permet alors de réaliser une collision entre une nouvelle goutte d'eau et la colonne d'eau lorsqu'elle est à son maximum.

![Droplet 1](../images/projet/droplet/droplet_1.jpg)
![Droplet 2](../images/projet/droplet/droplet_2.jpg)
![Droplet 3](../images/projet/droplet/droplet_3.jpg)
![Droplet 4](../images/projet/droplet/droplet_4.jpg)
![Droplet 5](../images/projet/droplet/droplet_5.jpg)

La bouteille est fixée sur un pied photo à l'aide d'une fixation rapide. Cela permet de faire un réglage précis en hauteur du départ de la goutte d'eau. Grace au système de fixation rapide, la bouteille peut être remplacée rapidement par une autre avec un liquide différent par exemple.

L'électrovanne est pilotée par une carte Arduino et les commandes sont faites par un ordinateur via le port série. La carte Arduino possède aussi un clavier et un afficheur LCD pour commander et visualiser le timing.

Tous les éléments de fixation ont été réalisés à l'aide d'une imprimante 3D.

## Version N°2 du water droplet

![Droplet 6](../images/projet/droplet/droplet_6.jpg)

Nouvelle version avec un boîtier entièrement imprimé en 3D. Celui-ci pilote une électro-vanne, un flash et un appareil photo. Il fonctionne en mode Open Flash.

Ci-dessous quelques photos réalisées avec avec ce nouveau boîtier.

![Droplet 7](../images/projet/droplet/droplet_7.jpg)