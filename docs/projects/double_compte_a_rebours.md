# Double compte à rebours

![Image de présentation](../images/projet/double_compte_a_rebours/double-countdown.jpg)

## Contexte

Mais à quoi ça sert ? À l'origine, ce double compte à rebours a été utilisé pour minuter des temps de paroles. Il y a un minuteur principal (en haut) configuré pour un temps donné qui détermine la durée totale du temps de parole. Le second minuteur (en bas) est individuel. D'une durée plus courte, il peut repartir à zéro dès que l'on a appuyé sur le bouton. Cela permet de proposer la même durée maximale de parole pour chacun des protagonistes. 

Une fois le minuteur individuel terminé, il faut laisser parler la personne suivante.
Une fois le minuteur général terminé, plus personne ne parle.

Ce projet a pu être détourné lors de l'organisation d'un escape game : le minuteur principal affichait le temps total, et le deuxième afficheur permettait d'afficher un code nécessaire à ouvrir un cadena... Le code n'apparaissait que sous certaines conditions (l'appui sur le bouton par exemple...).

Pour les 2 situations, une fois un des minuteurs terminés, une sonnerie retentie.

## Matériel nécessaire

- 2 afficheurs 7 segments
- un micro-contrôleur (AtTiny 85)
- un bouton poussoir
- un piezo (pour avoir une mélodie à la fin du temps écoulé)
- un potentiomètre (pour régler le volume)
- 2 Connecteurs jack femelles (un sur le boîtier principal, et un sur le bouton)
- Un câble jack (pour relier le bouton poussoir au boîtier)

## Réalisation

### Prototypage

![Schématique](../images/projet/double_compte_a_rebours/schematique.png)
![Prototypage](../images/projet/double_compte_a_rebours/arduino.png)

### Réalisation d'une carte imprimée

![PCB](../images/projet/double_compte_a_rebours/pcb.png)

![Intérieur 1](../images/projet/double_compte_a_rebours/interieur.jpg)

### Bouton poussoir

Pour un aspect ludique, un bouton d'arrêt d'urgence a été utilisé en guise de bouton poussoir.

Les boutons d'arrêt d'urgence se verrouillent quand on appui dessus. Il faut donc bien penser à supprimer ce mécanisme pour obtenir un bouton poussoir classique.

## Conception 3D

Utilisation de fusion 360 pour réaliser ce boîtier

![Modèle 3D](../images/projet/double_compte_a_rebours/model3D.png) ![Boîte 1](../images/projet/double_compte_a_rebours/boite1.jpg) ![Boîte 2](../images/projet/double_compte_a_rebours/boite2.jpg)

## Résultat final

![Intérieur 2](../images/projet/double_compte_a_rebours/interieur2.jpg)

