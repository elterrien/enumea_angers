# Réalisation du robot Wall-E en impression 3D

Wall-E est le titre d'un film d'animation dont le personnage principal est un robot cubique du même nom. 

Des passionnés ont mis à disposition les modèles pour reproduire le robot par impression 3D, l'équiper de moteur, d'une caméra et d'un système de pilotage. 

Tous les fichiers pour imprimer le robot sont disponibles gratuitement sur le site thingiverse [Lien vers le projet](https://www.thingiverse.com/thing:3703555).


## Impression 3D du corps du robot Wall-E

![Corsp Wall-e impression 3D](../images/projet/wall_e/corps_wall_e_1.jpg)
![Corsp Wall-e impression 3D](../images/projet/wall_e/corps_wall_e_2.jpg)
![Corsp Wall-e impression 3D](../images/projet/wall_e/corps_wall_e_3.jpg)

## Impression et assemblage des chenilles

![Corsp Wall-e impression 3D](../images/projet/wall_e/assemblage_chenilles.JPG
)

Vidéo des chenilles en fonctionnement : <https://youtube.com/shorts/Ia6VbmIOJ80?feature=share>

## Peinture des pièces imprimées en 3D

![peinture impression 3D](../images/projet/wall_e/test_peinture_1.jpg)
![peinture impression 3D](../images/projet/wall_e/test_peinture_2.jpg)
![peinture impression 3D](../images/projet/wall_e/test_peinture_3.jpg)

