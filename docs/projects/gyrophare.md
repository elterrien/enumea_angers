# Gyrophare

![gyrophare](../images/projet/gyrophare/ambiance.png)

## Description du projet

Il s'agit simplement d'un ruban de led, relié à un micro-contrôleur wifi, qui affiche une animation. Ce gyrophare peut être utilisé en tant qu'avertisseur lumineux, en étant relié à un système domotique, pour alerter de l'ouverture de porte ou fenêtre.

Les animations de base permettent aussi de faire de ce gyrophare un objet de décoration, les animations s'enchaînant les unes après les autres.

![alarme](../images/projet/gyrophare/alarme.jpg)

## Principe de fonctionnement

Seul, le gyrophare a un fonctionnement limité, qui ne lui permettrait que de jouer des animations une fois allumé.

Le micro-contrôleur étant relié à Internet, il est possible de lui ajouter un serveur web (comme [l'afficheur de télétravail](../afficheur_teletravail/)) par exemple. Des commandes pourraient alors être envoyés par ce biais.

L'autre possibilité, tel qu'il est utilisé actuellement, est d'être relié à un système domotique. A l'identique du serveur web, une fois connecté, les animations affichées peut dépendre des scénarios domotiques.

## Aspects techniques

Rien de particulier à signaler niveau matériel, il s'agit d'un simple ruban de led directement branché au micro-contrôleur (avec un condensateur de protection). Le ruban de led est alimenté en courant directement par le câble d'alimentation (les 5v du cable USB alimentent le rubant de led et le micro-contrôleur). Reste le branchement entre le micro-contrôleur et le ruban de led pour faire circuler les données (2 connexions ici).

Niveau logiciel, outre la connexion wifi indispensable pour rendre ce gyrophare connectée, c'est le protocole MQTT qui a été choisi pour l'association au système domotique.

Les animations du ruban de led sont directement celles issues de la librairie FastLed.

![interieur](../images/projet/gyrophare/PCB_installe.jpg)

## Matériel utilisé

- Un ESP8266
- Un ruban de led APA102
- Un condensateur
- un connecteur USB pour l'alimentation


## Conception 3D

les 2 parties du boîtier ont été réalisés sous OpenSCad. L'impression du couvercle a nécessité l'utilisation d'un fil translucide pour laisser passer la lumière plus simplement.

![3D_Base](../images/projet/gyrophare/3D_base.png)
![3D_Couvercle](../images/projet/gyrophare/3D_couvercle.png)


![interieur](../images/projet/gyrophare/interieur.png)