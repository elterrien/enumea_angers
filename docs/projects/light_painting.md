# Light Painting : photographie

Le light Painting réalisé avec une baguette lumineuse

La baguette lumineuse se compose de 280 leds RVB WS2812, elle fait 2 mètres de haut et elle est pilotée par une carte Arduino Mega avec un lecteur de carte SD.

La carte SD contient l'image à intégrer dans le paysage de nuit.

Technique de prise de vue

L'appareil photo est placé en pose longue pour prendre une photo de nuit et la baguette lumineuse est déplacée perpendiculairement à l'axe de l'appareil photo.

![light painting](../images/projet/light_painting/light_painting_1.webp)
![light painting](../images/projet/light_painting/light_painting_2.webp)
![light painting](../images/projet/light_painting/light_painting_3.webp)