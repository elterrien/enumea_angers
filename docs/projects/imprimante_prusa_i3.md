# Fabrication d'une imprimante Prusa i3 RepRap

L'imprimante 3D est un élément incontournable dans un club de robotique. Elle permet de réaliser rapidement des pièces en plastique. Le choix c'est donc porté sur une imprimante RepRap Prusa i3 pour son très faible coût de mise en oeuvre. Et premier avantage de cette imprimante, elle est auto-réplicable, c'est à dire que toutes les pièces en plastique qui la compose peuvent être imprimées à partir d'une autre imprimante.

Cette page décrit en plusieurs phases la réalisation d'une deuxième imprimante RepRap. La première (photo ci-dessous) fonctionne depuis 2 ans et donne la même précision qu'au début. 

![Imprimante Prusa i3](../images/projet/imprimante_prusa/imprimante_3d_%20prusa_i3.png)

## Conception du chassis de l'imprimante sur Sketchup

Le châssis est réalisé en médium (mdf épaisseur 18mm)

Le châssis a été dessiné  sur un logiciel de dessin 3D pour vérifier que toutes les pièces plastiques se montent bien.

Le logiciel de dessin utilisé pour dessiner l'imprimante est Sketchup (version gratuite).

Si vous êtes intéressé par cette réalisation vous pouvez me demander le fichier Sketchup en utilisant la messagerie dans la page d'acceuil.

![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante.webp)

<figure markdown>
  ![Rail Time Lapse](../images/projet/imprimante_prusa/chassis_imprimante_1.webp)
  <figcaption>réalisation des plans de montage à partir de Sketchup</figcaption>
</figure>

![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante_2.webp)
![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante_3.webp)
![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante_4.webp)
![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante_5.webp)
![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante_6.webp)
![Imprimante Prusa i3](../images/projet/imprimante_prusa/chassis_imprimante_7.webp)