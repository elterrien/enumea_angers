# Mesure de la consommation électrique avec Linky

Le compteur Linky est un compteur permet de mesurer la consommation électrique en temps réel. 
Il est alors possible de récupérer les données de consommation en se connectant aux bornes Télé-Information-Client (TIC) 
du compteur.

## Carte électronique pour se connecter au compteur Linky

Pour se connecter au compteur Linky, il faut utiliser une carte électronique qui permet de se connecter aux bornes TIC.
La carte électronique conçue par les membres de l'association est très simple.

Elle est composée d'un optocoupleur et de deux résistances. L'optocoupleur permet d'isoler le compteur Linky.

Pour réaliser les tiges de connexion, nous avons utilisé des cordes à piano. 
Elles s'insèrent dans les bornes du compteur Linky. 

### Schémas de la carte électronique pour se connecter au compteur Linky

![Cirduit électronique](../images/projet/linky_tic/circuit_electrique.png)

![Schema PCB](../images/projet/linky_tic/schema_pcb_2.png)


### PCB de la carte électronique pour se connecter au compteur Linky


![PCB](../images/projet/linky_tic/pcb.jpg)


## Dessins 3D du boitier

Pour fabriquer le boiter avec une imprimante 3D, il faut réaliser les dessins 3D avec un logiciel.  

L'ensemble se compose d'un boitier et d'un couvercle.

Sa taille est adaptée pour pouvoir y loger la carte électronique et épouser la forme du compteur Linky.


|                                Boitier                                |                                 Couvercle                                 | Boitier Imprimé en 3D avec la carte électronique |
|:---------------------------------------------------------------------:|:-------------------------------------------------------------------------:|:------------------------------------------------:|
| ![Boitier Linky TIC <](../images/projet/linky_tic/boitier_linky.png)  | ![Couvercle Linky TIC <](../images/projet/linky_tic/couvercle_linky.png)  |![Boitier et PCB <](../images/projet/linky_tic/boitier_pcb.png)|


### Fichiers 3D pour fabriquer le boitier avec une imprimante 3D

Télécharger les fichiers .step du projet :

* boitier : [Linky_Box](../images/projet/linky_tic/Linky_Box-v6.step)
* couvercle : [Linky_Couvercle](../images/projet/linky_tic/Linky_Couvercle-v4.step)

Télécharger les fichiers .f3d (Fusion 360)

* boitier : [Linky_Box](../images/projet/linky_tic/Linky_Box-v6.f3d)
* couvercle : [Linky_Couvercle](../images/projet/linky_tic/Linky_Couvercle-v4.f3d)


