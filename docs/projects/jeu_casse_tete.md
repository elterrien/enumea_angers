# Jeu casse-tête

## Description du projet

Il s'agit d'un jeu de casse tête, où l'objectif est de trouver un code à quatre chiffres à l'aide de branchement.

![Boitier sans fil](../images/projet/jeu_casse_tete/boitier_sans_fil.png)
![Boitier avec fils](../images/projet/jeu_casse_tete/boitier_avec_fils.png)


## Principe de fonctionnement

Le casse tête est divisé en 3 parties :

- En haut à gauche des LED d'indication de l'état
- En haut à droite la position
- En bas les chiffres à affecter à la bonne position

Une fois les fils branchés, il faut appuyer sur le bouton pour vérifier le code. Pour chaque position :

- La led verte clignotte si le chiffre est correct et à la bonne position
- La led orange clignotte si le chiffre est correct, mais à la mauvaise position
- La led rouge clignotte si le chiffre n'est pas correct

L'opération est renouvellée pour chaque position, les leds clignottent donc au total 4 fois.

## Aspect technique

Chaque position est relié à une entrée analogique de l'arduino. Une fois connectée à un chiffre, le courant passe par un certain nombre de résistances (de 1 à 10 résistances) ce qui provoque une chute de tension. La valeur de la tension obtenue par l'entrée analogique de l'arduino permet de déterminer le nombre de résistances traversé et donc le chiffre connecté à la position.

## Matériel utilisé

- Un boîtier de récup'
- Des fiches bananes femelles (x14)
- Des fiches bananes mâles (x8)
- Des résistances
- Des leds
- Un bouton
- Un arduino Nano
- Un interrupteur

## Prototypage

![Breadboard](../images/projet/jeu_casse_tete/breadboard.png)

_Les fils en pointillés blanc/couleur représentent la connexion entre les positions et les chiffres. Dans cet exemple, le code renseigné est 0249_

### Installation dans le boîtier

![PCB_1](../images/projet/jeu_casse_tete/PCB_1.png)
![PCB_2](../images/projet/jeu_casse_tete/PCB_2.png)
![Installation finale](../images/projet/jeu_casse_tete/interieur.png)
