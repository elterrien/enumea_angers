# Jeu de mémoire

![projet](../images/projet/jeu_memoire/projet.jpg)

## Description du projet

Il s'agit d'un simple jeu de mémoire. Les boutons s'allument dans un ordre, et il faut appuyer sur les boutons dans le même ordre.

Au début du jeu, seul un bouton s'allume. Un nouveau s'allume à chaque séquence réussie. Si on se trompe de bouton, c'est perdu.

## Principe de fonctionnement

Au démarrage du programme, une séquence aléatoire est générée de manière à avoir une séquence unique à chaque jeu. La séquence génère 50 positions (blanc/vert/bleu/rouge) et commence par la première position. Dès que l'ordre des appuis sur les boutons correspond aux positions, on rajoute une position de la séquence.

La seule vérification qui est faite, c'est qu'il n'y ai pas 2 fois le même bouton à appuyer successivement.

## Matériel utilisé


- un atmega328p (même microcontrôleur que dans un Arduino Uno). L'utilisation du microcontrôleur seul permet de réduire la consommation électrique pour ce projet, qui est alimenté par des piles
- 4 boutons lumineux style Arcade en 5V
- Un buzzer piezo, pour accompagner le signal lumineux
- un potentiomètre, pour régler le "volume" du piezo
- un interrupteur
- un convertisseur 5V pour augmenter la tension des piles
- des résistances

![schéma](../images/projet/jeu_memoire/PCB_AtMega328p.png)
![interieur](../images/projet/jeu_memoire/interieur.jpg)

## Conception 3D

Le modèle 3D a été réalisé sous Openscad (avec modélisation des composants pour voir le rendu final)

![3D Vide](../images/projet/jeu_memoire/3D_vide.png)

![3D Composants](../images/projet/jeu_memoire/3D_composants.png)