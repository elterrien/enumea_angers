# Anémomètre connecté Sigfox

Passée la mode du handspiner, le modèle Triskel imprimé depuis Thingiverse (thing:2090360) est reconverti en anémomètre. Un tout petit aimant est inséré près du roulement central, afin de faire contact sur la capsule ‘reed’ insérée dans la partie fixe.

![Anémomètre impression 3D](../images/projet/anemometre/anemo_1.jpg) ![Anémomètre impression 3D](../images/projet/anemometre/anemo_2.jpg)


Le tout est imprimé à partir de différents modèles (things:41367 & 2849562) retouchés et collés sur le boîtier (thing:2703383) pour l’antenne et la carte, alimentée en 3V par 2 piles pour qq mois en pleine nature. Un alimentation solaire ou … éolienne pourra ensuite améliorer le dispositif…

![Anémomètre impression 3D](../images/projet/anemometre/anemo_3.jpg) ![Anémomètre impression 3D](../images/projet/anemometre/anemo_4.jpg)

Pour la carte Arduino, le MKRFOX1200 est une plateforme très bien équipée du micro-contrôleur SAM D21 et d’un modem SIGFOX livré avec 2 ans d’abonnement pour expérimenter les services offerts par cette technologie.

La température  est directement  transmise par une fonction intégrée (température interne du ship), et le contact reed de l’anémomètre met simplement au niveau bas (GND) une entrée (D0) pour  déclencher une interruption et le réveil du ship qui transmet tous les quarts d’heure les 2 informations température et vent.

Le code peut être télécharger > [ici](https://f8385b48-0381-4d7f-8b3c-a24eed1babef.filesusr.com/ugd/96ef27_7a9aa3b7ff994bd0a8164bbeac61d79d.doc?dn=CodeArduino.doc)    (modifier l'extension du fichier doc en ino)

Le programme reste à finaliser car il réveille le ship et envoie un message à chaque coup de vent alors qu’il pourrait ne se réveiller que pour enregistrer les cycles de vent (revolutions)



Ensuite le système ‘backend SIGFOX’ propose de créer un compte pour la restitution des informations reçues dans la limite de 120 messages par jour, voire bi-directionnel mais ne permet pas de fonctionner en M2M, contrairement à ce qui devrait encore être possible en LORA ou FSK avec d’autres cartes. Cependant SIGFOX annonce une couverture remarquable et s’avère en effet très fiable en réception, avec des envois de mail en cas de perte (non-continuité des séquences) des messages reçus.
![Anémomètre impression 3D](../images/projet/anemometre/dash_sigfox_1.png)


L’association du dispositif est réalisée de façon bien sécurisée en suivant les tutos proposés sur le site. Ensuite il est possible de visualiser chaque message, dans leur contenu, leur qualité du signal, l’historique, et leur localisation (à 1 km près).

![Anémomètre impression 3D](../images/projet/anemometre/dash_sigfox_2.png)

Des callbacks permettent de configurer des alertes par mail, ou relayés sur des liens http pour une restitution plus personnalisée.

![Anémomètre impression 3D](../images/projet/anemometre/dash_sigfox_3.png)

Ainsi le retour des informations vers l’URL d’un site local hébergé sur un Raspberry3, et node-red nativement  installé sur la raspbian reçoit et interprète du ‘backend SIGFOX’ un ‘callback HTTP POST’ véhiculant les données par une petite formule en ‘json’ :

![Anémomètre impression 3D](../images/projet/anemometre/dash_sigfox_4.png)

Après installation du dashboard, la partie Node-red consiste à lier graphiquement les fonctions d’extracion des messages reçus depuis le backend SIGFOX et les mettre en forme de jauges ou histogrammes sur les différents écrans pré-formattés :

![Anémomètre impression 3D](../images/projet/anemometre/red_node.png)

Il reste une grande variété de fonctions à explorer dans cet environnement graphique mais le résultat sans une ligne de code est plutôt très bien rendu notamment sur smartphone. Le but est de construire une application sur smartphone ou internet utilisable par les membres du club d’aéromodélisme pour prendre  connaissance à distance des conditions météo sur le terrain de vol . En voici quelques captures d’écrans :

![Anémomètre impression 3D](../images/projet/anemometre/anemo_temp.png) ![Anémomètre impression 3D](../images/projet/anemometre/anemo_vitesse.png) ![Anémomètre impression 3D](../images/projet/anemometre/anemo_map.png)
