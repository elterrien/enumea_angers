# Afficheur de télétravail

## Description du projet

L'objectif de ce projet est de réaliser un afficheur de télétravail. Il permet de savoir si la personne travaillant est en réunion ou non.

L'idée est venue en découvrant le tutoriel d'[Adafruit](https://learn.adafruit.com/3d-printed-iot-on-air-sign-for-twitch/overview) où seul le panneau 3D "On Air" a été utilisé par rapport à ce projet

## Principe de fonctionnement

Le projet est découpé en 2 parties :

- Une partie "récepteur" qui permet d'afficher la (non) disponibilité.
- Une partie "émetteur", qui permet de choisir la (non) disponibilité.

L'émetteur se place dans le bureau et permet de choisir la couleur à l'aide de bouton. Le récepteur se place lui à l'extérieur du bureau et allume le ruban de led en fonction de la couleur choisie.

L'émetteur possède lui aussi une led RGB permettant d'avoir un retour sur la couleur affichée par le récepteur.

![Image Off](../images/projet/afficheur_teletravail/OnAirSignOff.jpg)

![Image Couleur Bleue](../images/projet/afficheur_teletravail/OnAirSignBleu.jpg)

![Image Couleur Verte](../images/projet/afficheur_teletravail/OnAirSignVert.jpg)

![Image Couleur Rouge](../images/projet/afficheur_teletravail/OnAirSignRouge.jpg)

## Matériel utilisé

### Récepteur

- Un micro-contrôleur wifi ESP8266 (ESP01)
- Un ruban de led
- Une batterie
- Un module de charge (TP4056)
- Un régulateur de tension LD33v
- Un interrupteur
- Des condensateurs

![Schéma récepteur](../images/projet/afficheur_teletravail/OnAirSignReceiver.png)

### Émetteur

- Un micro-contrôleur wifi ESP8266 (Lolin D1 Mini)
- Des boutons
- Une led RGB

![Schéma récepteur](../images/projet/afficheur_teletravail/OnAirSignEmitter.png)

## Aspect technique

### Récepteur

Une fois allumé, le récepteur se connecte au wifi domestique et créé un mini serveur web semblable à un serveur d'API Rest.

Un seul endpoint est nécessaire pour indiquer s'il faut allumer le ruban de led avec la couleur en paramètre (à l'aide de la valeur _hue_ de la bibliothèque `fastled`), ou bien s'il faut l'éteindre.

Le récepteur est alimenté par une batterie lui permettant de rester allumé pendant une dizaine d'heure environ (à recharger après une grosse journée de réunions !)

### Émetteur

Une fois allumé, l'émetteur se connecte lui aussi au wifi domestique. Lors de l'appui sur un bouton, le serveur web du récepteur est appelé, avec le code de la couleur correspondant au bouton appuyé. Si (et seulement si) le récepteur reçoie et traite l'appel de l'émetteur (à l'aide d'un code retour 200), alors le retour s'allume lui aussi de la même couleur. Ce fonctionnement permet de s'assurer que le récepteur fonctionne correctement.

## Conception 3D

Le logiciel Freecad a été utilisé pour modéliser toutes les pièces propres au projet.

### Récepteur

Le récepteur est constitué de plusieurs étages, empilés les uns aux autres.

À trouver sur [Thingiverse](https://www.thingiverse.com/thing:1506862) (projet d'Adafruit) :

- Un étage constitué d'une plaque translucide (imprimée en 3D) 
- Un étage constitué de la plaque "ON AIR"

Réalisation propre au projet :

- Un étage pour les composants
- Un étage pour le ruban de LED
- Un bloc pour l'ensemble

L'étage lié au ruban de LED a été prévu pour que le ruban ne soit pas face au panneau translucide, mais que les 2 parties soient face à face de manière à se refléter sur le blanc des pièces imprimées. De cette manière la couleur est plus homogène et le ruban n'est pas visible.

![étage pour les composants](../images/projet/afficheur_teletravail/OnAirSign3DComposants.png)

![Résultat étage pour les composants](../images/projet/afficheur_teletravail/OnAirSignReceiverComposants.jpg)

![étage pour le ruban de LED](../images/projet/afficheur_teletravail/OnAirSign3DLeds.png)

![Résultat étage pour le ruban de LED](../images/projet/afficheur_teletravail/OnAirSignReceiverLeds.jpg)

![Résultat étage pour le ruban de LED 2](../images/projet/afficheur_teletravail/OnAirSignReceiverLeds2.jpg)

![Résultat étage pour le ruban de LED 3](../images/projet/afficheur_teletravail/OnAirSignReceiverLedsRougeSansPanneau.jpg)

![Résultat étage pour le ruban de LED 4](../images/projet/afficheur_teletravail/OnAirSignReceiverLedsRougeAvecPanneau.jpg)

![étage pour l'ensemble](../images/projet/afficheur_teletravail/OnAirSign3DContour.png)

![Résultat Final 1](../images/projet/afficheur_teletravail/OnAirSignReceiver34Cote.jpg)

![Résultat Final 2](../images/projet/afficheur_teletravail/OnAirSignReceiverFace.jpg)

### Émetteur

Toujours en cours :(

## Sources

[Projet d'Adafruit](https://learn.adafruit.com/3d-printed-iot-on-air-sign-for-twitch/overview)