# Rail pour réaliser des photographies en time lapse

La partie fixe est composée d'une règle de maçon de 1500 mm sur laquelle est fixée d'un bout à l'autre une courroie crantée. En dessous du rail sont vissées deux fixations rapides permettant de se fixer sur 2 trépieds.

Les pièces constituant la partie mobile sont en PLA et ont été réalisées à l'aide d'une l'imprimante 3D. La motorisation de la partie mobile est faite par un moteur pas à pas équipé d'une poulie crantée. Les 4 roues sont des roulements fixés sur des tiges lisses et 4 autres roulements servent à guider le chariot. Deux capteurs permettent de détecter les deux extrémités.

La partie commande est composée d'une carte Pro Mini Arduino et d'un driver DRV8825 permettant d'avoir une précision de déplacement de 0,1mm. La programmation du Time Lapse se fait à l'aide d'un ordinateur et le rail est entièrement autonome après cette programmation. 

Le chariot accepte soit un appareil photo et dans ce cas la carte Arduino déclenche automatiquement les photos via un opto coupleur et un cordon relié à l'appareil ou soit l'on peut mettre une caméra Gopro qui elle sera piloter par un module Wifi ESP8266.

![Rail Time Lapse](../images/projet/rail_time_lapse/rail_time_lapse_1.webp)
![Rail Time Lapse](../images/projet/rail_time_lapse/rail_time_lapse_2.webp)

<figure markdown>
  ![Rail Time Lapse](../images/projet/rail_time_lapse/rail_time_lapse_3.webp)
  <figcaption>roulement pour le guidage</figcaption>
</figure>

<figure markdown>
  ![Rail Time Lapse](../images/projet/rail_time_lapse/rail_time_lapse_4.webp)
  <figcaption>vue des roues et du passage de la courroie</figcaption>
</figure>

<figure markdown>
  ![Rail Time Lapse](../images/projet/rail_time_lapse/rail_time_lapse_5.webp)
  <figcaption>pièce pour la fixation de la courroie</figcaption>
</figure>

<figure markdown>
  ![Rail Time Lapse](../images/projet/rail_time_lapse/rail_time_lapse_6.webp)
  <figcaption>pièce pour la fixation de la courroie</figcaption>
</figure>


