Title: Association d'électronique et numérique à Angers
Summary: Association d'entraide et de partage de connaissances liées à l'électronique, le numérique, la robotique et l'impression 3D.


# Espace Numérique & Electronique Angevin
Créée en 2016, l'association a pour objet l'initiation des adhérents à la robotique, l'électronique, la programmation, aux objets connectés et aux nouvelles technologies.

## A qui s'adresse l'association ?

L'association est ouverte à tous que vous soyez **débutant** ou **expert** dans un domaine. Vous souhaitez réaliser un projet personnel ou particier aux projets de l'association ?
Contactez-nous à l'adresse suivante : <enumeas@gmail.com>

## Adresse et horaires d'ouverture

L'association dispose d'une salle au sein de la maison de quartier Le Trois Mâts, place des Justices à Angers.

**L'association est ouverte :**

- le mercredi de 19h à 21h
- le samedi de 10h à 12h

!!! info

    L'association est fermée pendant les vacances scolaires.

## Pourquoi adhérer à l'association ?

Vous accédez à une **mine de savoir collectif** en électronique, impression 3D, dessin 3D, robotique, programmation informatique...

Du matériel, une imprimante 3D et des cartes programmables sont à disposition des membres.

La cosition annuelle est de 40€.