# Atelier d'initiation à la programmation Python à Angers

Bienvenue sur la page de présentation de notre atelier d'initiation à la programmation Python à Angers. 
Si vous êtes intéressé par la programmation informatique, vous êtes au bon endroit ! 

Cet atelier est conçu pour les débutants (à partir de 13 ans, les plus jeunes devront être accompagnés d'un adulte) qui 
souhaitent découvrir le langage de programmation Python, l'un des langages les plus populaires et accessibles. 

Le langage Python est au programme de l'éducation nationale depuis la rentrée 2019.

## Contenu de l'atelier d'initiation à la programmation Python

Notre atelier est organisé par des bénévoles passionnés de programmation informatique, membres de l'association ENUMEA. 
Pendant l'atelier, vous apprendrez les bases de la programmation Python, telles que :

* La syntaxe de base : vous apprendrez les principes de base de la syntaxe Python, notamment les variables, les boucles et les conditions.
* Les fonctions : vous découvrirez comment utiliser les fonctions en Python pour organiser et réutiliser votre code.
* Les modules : vous apprendrez comment utiliser les modules en Python pour étendre les fonctionnalités de votre code.
* Les projets pratiques : vous travaillerez sur des projets pratiques, tels que la création d'un jeu

Notre atelier est ouvert à tous, quel que soit votre niveau de connaissances en programmation. 


## Comment participer à l'atelier d'initiation à la programmation Python ?

Si vous êtes intéressé par cet atelier, n'hésitez pas à nous contacter pour plus d'informations à [enumeas@gmail.com](mailto:enumeas@gmail.com). 
Nous proposons des sessions régulières tout au long de l'année. Les places sont limitées, alors inscrivez-vous dès 
maintenant pour ne pas manquer cette opportunité !

Le prochain atelier aura lieu le **samedi 24 février 2024 de 10h à 12h** à la maison de quartier Le Trois Mâts, place des Justices à Angers.

**Pour vous inscrire, cliquez sur le bouton ci-dessous :**

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/espace-numerique-et-electronique-angevin/evenements/initiation-a-la-programmation-python-1/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>


La programmation Python est un langage accessible et puissant, qui peut vous ouvrir de nombreuses portes dans le monde 
de l'informatique. Nous sommes convaincus que notre atelier d'initiation est un excellent moyen de découvrir les bases 
de ce langage et de commencer à explorer ses possibilités. Nous espérons vous voir bientôt dans notre atelier !