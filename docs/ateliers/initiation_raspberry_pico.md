# Atelier d'initiation à la programmation Micropython sur Raspberry Pico

**Découvrez la programmation sur un microcontrôleur facile à utiliser !**

Vous êtes intéressé par la programmation et vous voulez vous lancer dans l'aventure des microcontrôleurs ? 
Nous vous proposons un atelier d'initiation à la programmation Micropython sur Raspberry Pico !

## Qu'est-ce que le Raspberry Pico ?

Le Raspberry Pico est un microcontrôleur compact et économique développé par la fondation Raspberry Pi. 
Il est programmable en Micropython et C++ et permet de réaliser une grande variété de projets électroniques, 
tels que des capteurs, des contrôleurs de moteurs et des dispositifs d'interface utilisateur.

## Contenu de l'atelier d'initiation à la programmation Micropython sur Raspberry Pico

**Au programme de l'atelier :**

* Présentation de Raspberry Pico et de Micropython
* Installation et configuration de l'environnement de développement
* Apprentissage des bases de la programmation en Micropython
* Réalisation d'un projet concret avec Raspberry Pico

## A qui s'adresse cet atelier programmation ?

Que vous soyez débutant en programmation ou que vous ayez déjà des connaissances dans le domaine, cet atelier est 
adapté à tous les niveaux (à partir de 13 ans ou accompagné d'un parent). Nous vous guiderons pas à pas pour 
vous permettre de comprendre les principes de la programmation sur un microcontrôleur et de réaliser votre premier projet.


## Comment participer à l'atelier d'initiation à la programmation Micropython sur Raspberry Pico ?

Si vous avez des questions ou si vous souhaitez obtenir plus d'informations sur l'atelier, n'hésitez pas à nous contacter 
par email à l'adresse suivante : [enumeas@gmail.com](mailto:enumeas@gmail.com). Nous proposons des sessions régulières tout au 
long de l'année.

Nous sommes impatients de partager notre passion pour la programmation et de vous accompagner dans cette nouvelle expérience !