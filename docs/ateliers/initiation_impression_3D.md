# Atelier d'initiation à l'impression 3D à Angers

Bienvenue sur la page de présentation de notre atelier d'initiation à l'impression 3D. 
Vous êtes intéressé par cette technologie et souhaitez en savoir plus ? Vous êtes au bon endroit ! 
Dans cet atelier, vous apprendrez les bases de l'impression 3D et pourrez vous initier à 
cette pratique fascinante.

## Contenu de l'atelier d'initiation à l'impression 3D

Notre atelier est conçu pour les débutants qui souhaitent apprendre les bases de l'impression 3D. 
Pendant l'atelier, vous apprendrez :

* Les principes de base de l'impression 3D : comment fonctionne une imprimante 3D, quels sont les 
types de matériaux utilisés, comment créer un modèle 3D, etc.
* La modélisation 3D : vous découvrirez les principes de base de la modélisation 3D à l'aide de 
logiciels tels que SketchUp Make ou FreeCAD.
* La préparation du modèle : vous apprendrez comment préparer le modèle pour l'impression, 
notamment comment le découper et le positionner sur le plateau d'impression avec le logiciel CURA.
* L'impression : vous pourrez enfin imprimer votre modèle et voir comment la machine fonctionne en direct.

Notre atelier est animé par des bénévoles de l'association ENUMEA qui vous guideront tout au long de 
la séance. Nous fournissons également tout le matériel nécessaire pour l'atelier.

## Comment s'inscrire à l'atelier d'impression 3D ?

Si vous êtes intéressé par cet atelier, n'hésitez pas à nous contacter par mail [enumeas@gmail.com](mailto:enumeas@gmail.com) pour 
plus d'informations. Nous proposons des sessions régulières tout au long de l'année. Les places sont limitées, 
alors inscrivez-vous dès maintenant pour ne pas manquer cette opportunité !

## Pourquoi s'initier à l'impression 3D ?

L'impression 3D est une technologie passionnante et en constante évolution. Nous sommes convaincus que 
notre atelier d'initiation est un excellent moyen de découvrir les bases de cette pratique et de commencer à 
explorer ses possibilités. Nous espérons vous voir bientôt dans notre atelier.